#!/usr/bin/python
# -*- coding: utf-8 -*-
import requests
from requests.auth import HTTPBasicAuth
import re
import io
import time
import progress
from progress.bar import Bar
import json

# Write issues collected to file. JSON
# IF SPECIFIED THE SCRIPT WILL EXPECT YOU TO MAKE THE FILE FIRST
# place this file in the root directory
OUTPUT = ""

# used to combine jira ticket id and summary to gitlab title
DELIMETER = ' - '
JIRA_URL = 'https://yourjira.atlassian.net/'
JIRA_ACCOUNT_AUTH = 'base64string=user:pass'
# what space your project belongs to.
# this id usually prepends the ticket id. 
# example: ABC-1234 (ABC is the project space)
JIRA_PROJECT_SPACE = ""
# this is optional and used to better define the project wall you want tickets from
# Leave blank if not needed or unknown
# Field to look for
JIRA_CUSTOM_FIELD_IDENTIFIER = "customfield_1"
# Field value expected
JIRA_CUSTOM_FIELD_IDENTIFIER_VALUE = "Field Value Expected"
# Gitlab settings
# location of your self-hosted git
GITLAB_URL = 'https://yourgit.gitlab.com/'
# Gitlab token obtained from creating an access token in user settings
GITLAB_TOKEN = 'GITLABTOKEN'
# numeric project ID
GITLAB_PROJECT_ID = 0

# if using self-signed certificate set to false
VERIFY_SSL_CERTIFICATE = True

issues = []
start_at = 0
all_collected = 0
issues_already_pulled = 0
total = 1

# todo clean this up
jira_url_domain = "{0}rest/api/2/search".format(JIRA_URL)
jira_url_query = "?jql=project={0}".format(JIRA_PROJECT_SPACE)
end_url = jira_url_domain + jira_url_query

# make this call only to get the total
total = requests.get(end_url, verify=VERIFY_SSL_CERTIFICATE, headers={'Content-Type': 'application/json', 'Authorization': 'Basic ' + JIRA_ACCOUNT_AUTH}).json()

recBar = Bar('Receiving', max=total['total'], suffix='%(index)d/%(max)d - %(percent).1f%% - %(eta)ds')
while all_collected == 0:
    jira_url_filter = "+&startAt={0}&maxResults=500".format(start_at)
    end_url = jira_url_domain + jira_url_query + jira_url_filter
    jira_issues = requests.get(end_url,
                               verify=VERIFY_SSL_CERTIFICATE,
                               headers={'Content-Type': 'application/json',
                                        'Authorization': 'Basic ' + JIRA_ACCOUNT_AUTH
                                        }).json()

    for ji in jira_issues['issues']:
        # If special search field:value set
        if ji['fields'] and JIRA_CUSTOM_FIELD_IDENTIFIER != "" and JIRA_CUSTOM_FIELD_IDENTIFIER_VALUE != "":
            if ji['fields'][JIRA_CUSTOM_FIELD_IDENTIFIER]:
                if ji['fields'][JIRA_CUSTOM_FIELD_IDENTIFIER]['value'] == JIRA_CUSTOM_FIELD_IDENTIFIER_VALUE:
                    issues = issues + [ji]
        else:
            issues = issues + [ji]


    total_issues_pulled = len(jira_issues['issues'])
    total_all_issues = jira_issues['total']
    if total_issues_pulled + issues_already_pulled >= total_all_issues:
        all_collected = 1
    else:
        issues_already_pulled += total_issues_pulled

    start_at = issues_already_pulled
    if start_at == total_all_issues:
        all_collected = 1

    recBar.next(total_issues_pulled)

if OUTPUT != "":
    with open(OUTPUT, 'w') as outfile:
        json.dump(issues, outfile)

recBar.finish()

#
# Start the import
#
bar = Bar('Importing', max=len(issues), suffix='%(index)d/%(max)d - %(percent).1f%% - %(eta)ds')
for issue in issues:
    counter = 0
    counter += 1

    assignee = ''
    reporter = ''
    if issue['fields']['assignee']:
        assignee = issue['fields']['assignee']['name']
    if issue['fields']['reporter']:
        reporter = issue['fields']['reporter']['name']

    issue_status = issue['fields']['status']['name'].upper()

    # TODO Make these configureable
    if issue_status == "READY FOR DEV":
        issue_status = "READY FOR DEV"
    elif issue_status == "IN DEV":
        issue_status = "IN DEV"
    elif issue_status == "IN PROGRESS":
        issue_status = "IN DEV"
    elif issue_status == "READY FOR QA":
        issue_status = "READY FOR QA"
    elif issue_status == "IN QA":
        issue_status = "IN QA"
    elif issue_status == "READY FOR STAGING":
        issue_status = "STAGING"
    elif issue_status == "STAGING":
        issue_status = "STAGING"
    elif issue_status == "READY FOR SHOWCASE":
        issue_status = "READY FOR PRODUCTION"
    else:
        # Were skipping all other statuses
        issue_status = ""
        bar.next()
        continue

    # Check for existing match
    matches = requests.get(GITLAB_URL
                           + 'api/v4/projects/{0}/issues?search={1}'.format(GITLAB_PROJECT_ID, issue['fields']['summary']),
                           headers={'PRIVATE-TOKEN': GITLAB_TOKEN},
                           verify=VERIFY_SSL_CERTIFICATE).json()
    skip = 0
    existing_issue = 0
    for match in matches:
        if issue['fields']['summary'].strip() in match['title'].strip():
            if (issue['key']) in match['title']:
                if issue_status != "":
                    existing_issue = match
                    if issue_status in match['labels']:
                        skip = 1

    if (skip != 1):
        if existing_issue != 0:
            # TODO Only updates wall location for now
            data = {
                'labels': issue_status
            }

            # update the issue
            response = requests.put(
                GITLAB_URL + 'api/v4/projects/{0}/issues/{1}'.format(GITLAB_PROJECT_ID, existing_issue['iid']),
                headers={'PRIVATE-TOKEN': GITLAB_TOKEN},
                verify=VERIFY_SSL_CERTIFICATE,
                data=data
                )


        else:
            data = {
                'title': issue['key'] + DELIMETER + issue['fields']['summary'],
                'description': issue['fields']['description'],
                'created_at': issue['fields']['created'],
                'labels': issue_status
            }

            # create the issue
            gl_issue = requests.post(GITLAB_URL + 'api/v4/projects/%s/issues'
                                     % GITLAB_PROJECT_ID,
                                     headers={'PRIVATE-TOKEN': GITLAB_TOKEN},
                                     verify=VERIFY_SSL_CERTIFICATE,
                                     data=data,
                                     ).json()

    bar.next()

bar.finish()
#
print('imported %s issues from project Configure and Buy' % (len(issues)))
#
